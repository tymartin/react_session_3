import React from 'react';
import { BrowserRouter, Route} from 'react-router-dom'; 
import Navbar from './components/Navbar';
import Home from './components/Home'
import Product from './components/Product'
import Cart from './components/MyCart'
import Contact from './components/Contact'


function App() {
 return (
   <BrowserRouter> 
   	<React.Fragment> 
   		<Navbar/> 
   		<Route exact path="/" component={Home}/> 
   		<Route path="/products" component={Product}/> 
   		<Route path="/carts" component={Cart}/> 
   		<Route path="/contacts" component={Contact}/> 
   	</React.Fragment> 
    </BrowserRouter> 
  )
}
 
export default App;
