import React from 'react'; 


const Form = () => {
	return (
		<div className="row"> 
			<div className="col-md-6 bg-warning text-primary w-100">
				<hr/> 
				<h2><strong> Send Us A Message! </strong></h2> 
				<hr/>
				<strong>Name:</strong> 
				<input id="name" type="text" className="form-control <w-100></w-100>" />
				<strong>Email:</strong>  
				<input id="email" type="email" className="form-control w-100" />
				<strong>Message:</strong> 
				<input id="message" type="string" className="form-control w-100 h-25 mb-3" />
				<button className="btn btn-primary  mb-3"> Add message </button> 
			</div>
			<div className="col-md-6"> 
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d15441.661680089886!2d121.0449656!3d14.632344500000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sph!4v1580365559305!5m2!1sen!2sph" width="500" height="500" frameborder="0" allowfullscreen=""></iframe>
			</div> 
		</div> 
	)
}

export default Form; 