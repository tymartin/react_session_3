import React from 'react'; 
import Form from './Form'; 



const Contact = () => {
	return (
		<div className="container">
			<div className="row"> 
				<div className="col-12"> 
					<h1 className="text-center text-danger"> <strong>  Contact Us Page </strong> </h1>
					<hr/>
					<Form/> 
			</div>
		</div> 
	</div> 
		)
}

export default Contact