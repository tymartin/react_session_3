import React from 'react'; 
import { Link, NavLink} from 'react-router-dom'


const Navbar = () => {

let LogIN = true;
if (LogIN) {
	return (
		<nav className="navbar navbar-expand-lg navbar-dark bg-primary"> 
			<div className="container"> 
				<a className="navbar-brand" href="#">New React Store </a>
				<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="content">
					<span className="navbar-toggler-icon"> </span> 	 
				</button> 

				<div className="collapse navbar-collapse" id="content">
					<ul className="navbar-nav mr-auto">
						<li className="nav-item"> 
							<NavLink className="nav-link" to="/">Home</NavLink> 
						</li>  
						<li className="nav-item"> 
							<NavLink className="nav-link" to="/products">Products</NavLink> 
						</li>  
					</ul> 

					<ul className="navbar-nav ml-auto"> 
						<li className="nav-item"> 
							<NavLink className="nav-link" to="/carts">MyCart</NavLink> 
						</li> 
						<li className="nav-item"> 
							<NavLink className="nav-link" to="/contacts">Contact Us</NavLink> 
						</li> 
					</ul>
				</div> 
			 </div> 
		 </nav> 
		)
} else {
	return (
		<nav className="navbar navbar-expand-lg navbar-dark bg-danger"> 
			<div className="container"> 
				<a className="navbar-brand" href="#">New React Store </a>
				<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="content">
					<span className="navbar-toggler-icon"> </span> 	 
				</button> 
			 <h1> <strong>   You Are Logged Out!   </strong>  </h1> 
			 </div>
			</nav> 
		)
}

}

export default Navbar; 
